#!/bin/bash

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms of  #
#  the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses    #
#####################################################################################

#This script manages the data filtering toolchain and calls the necessary programs.
#It's only for testing purposes, since the programs will run on a Windows server
#anyway.
#Michael Reisecker, 2019-01

set -e #abort on errors

#The scripts are sourced so they can all access the following variables.

### SETTINGS ###
outfolder_meteo="../output" #could be fetched from io.ini
outfolder_plot="../plot"
data_qa_path="../../data_qa"

perform_data_qa=true
plot_results_in_r=false
convert_to_latin=false
################

main_script_path=$(pwd) #all paths that are cd'd into use this as the base directory

echo "- Invoking pre_run.sh..."
. ./pre_run.sh

echo "- Invoking processor..."
cd ..
./processor "$1" "$2"
cd $main_script_path

echo "- Invoking post_run.sh..."
. ./post_run.sh
