/***********************************************************************************/
/* Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol        */
/***********************************************************************************/
/* This is free software: you can redistribute and/or modify it under the terms of */
/* the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses   */
/***********************************************************************************/

/* SYNOPSIS: ./processor <start_date> <end_date> */

/*
 * Main control program for filtering meteo data with MeteoIO.
 * This program is used to read SMET data that was exported from the LWD Tirol & partners
 * weather database, filter it through MeteoIO and export it again to the database.
 * Michael Reisecker, 2019-01, 2019-02, 2019-03, 2019-04
 */

#include <cstring> //for pretty file open errors
#include <errno.h> //for file open errors
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include <meteoio/MeteoIO.h>

using namespace std;
using namespace mio; //MeteoIO's namespace

bool has_nodata(const MeteoData& md) { //does a data point contain any nodata values?
	for (size_t pp = 0; pp < md.getNrOfParameters(); ++pp) {
		if (md(pp) == IOUtils::nodata)
			return true;
	}
	return false;
}

int main(int argc, char** argv)
{
	cout << "*** This is " << __FILE__ << "." << endl;

	if (argc != 3) 
		throw invalid_argument("Unexpected number of input arguments: start and end date are required.");

	const string ini_path("input/io.ini");
	cout << "*** Reading configuration file..." << endl;
	Config cfg(ini_path);
	IOManager io(cfg);
	
	const double TZ = cfg.get("TIME_ZONE", "Input"); //get dates according to time zone
	Date sdate, edate;
	IOUtils::convertString(sdate, argv[1], TZ); //start date
	IOUtils::convertString(edate, argv[2], TZ); //end date

	vector< vector<MeteoData> > mvec;

	cout << "    [Looking at dates " << sdate.toString(Date::ISO) << " to " << edate.toString(Date::ISO) << "]" << endl;
	cout << "*** Reading data..." << endl;

	bool write_qa_log(false);
	cfg.getValue("DATA_QA_LOGS", "GENERAL", write_qa_log, IOUtils::nothrow);
	if (write_qa_log) {
		const string log_path = FileUtils::cleanPath(cfg.get("LOGFILE", "GENERAL")); //non-standard parameter
		cout << "    [Redirecting data QA logs to " << log_path << "]" << endl;
	
		streambuf *old = cout.rdbuf(); //couts from getMeteoData are data QA logs - redirect to file
		ofstream qa_logfile(log_path.c_str(), fstream::app); //clearing the logs is done by the scripts
		if (qa_logfile.fail()) {
			ostringstream ss;
			ss << "Could not open data qa logfile \"" << log_path << "\" for writing. Possible reason: "
			   << strerror(errno);
			throw AccessException(ss.str(), AT);
		}
		Date now;
		now.setFromSys();
		qa_logfile << "# Data quality logfile for run on " << now.toString(Date::ISO) << endl;
		cout.rdbuf(qa_logfile.rdbuf());

		io.getMeteoData(sdate, edate, mvec);
	
		cout.rdbuf(old); //restore stream to cout
		qa_logfile.close();
	} else { //if no logging is done we skip the buffer redirect
		io.getMeteoData(sdate, edate, mvec);
	}

	/*
	 * Here, we perform the interpolations. It is the control program's duty to request data on a given time step
	 * which will make MeteoIO interpolate all parameters that aren't there (and that are enabled in the ini).
	 * For example, SNOWPACK will resample everything to e. g. 15 minute steps.
	 * Since we don't do that, we call the internal interpolation routines directly.
	 */

	Meteo1DInterpolator interpol(cfg);
	MeteoData md;

	for (size_t ii = 0; ii < mvec.size(); ++ii) {
		const std::string stationHash( IOUtils::toString(ii) + "-" + mvec[ii].front().meta.getHash() );
		for (size_t jj = 0; jj < mvec[ii].size(); ++jj) {
			md = mvec[ii][jj]; //get meta values
			if ( has_nodata(mvec[ii][jj]) ) { //interpolate if any data point at this time step is nodata
				interpol.resampleData(mvec[ii][jj].date, stationHash, mvec[ii], md);
				mvec[ii][jj] = md; //md is the resampled data point
			}
		} //endfor jj
	}

	cout << "*** Writing output file(s)... " << endl;
	io.writeMeteoData(mvec); //output processed data for further use
	cout << "*** Output file(s) written." << endl;

	const double sampling_rate = 1. / (io.getAvgSamplingRate()*60);
	cout << "    [Average sampling rate of dataset: 1 per " //sanity check
		 << sampling_rate << " min.]" << endl;

	if (sampling_rate < 0)
		cout << "[W] No valid data found for time span." << endl;

	cout << "*** Done" << endl;
	return 0;
}

